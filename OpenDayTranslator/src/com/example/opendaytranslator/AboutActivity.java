package com.example.opendaytranslator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import com.kilobolt.framework.implementation.Copyright;

/**
 * Created by timur on 15/04/2014.
 */
public class AboutActivity extends Activity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        showMicrosoftInfo();
        showCopyright();

}

    public void showCopyright(){
        // Copyright, e.g. "Author1, Author2 and Author 3"
        String authors = getString(R.string.timur) + " " + getString(R.string.and) + " " + getString(R.string.said);
        Copyright copyright = new Copyright(this, authors);
        TextView tv = (TextView) findViewById(R.id.tvCopyrightAbout);
        tv.setText(copyright.getCopyright(this));
    }

    public void showMicrosoftInfo(){
        // Copyright, e.g. "Author1, Author2 and Author 3"
        String info = getString(R.string.api_info);

        TextView tv2 = (TextView) findViewById(R.id.tvMicrosoftAbout);
        tv2.setText(info);
        TextView tv3 = (TextView) findViewById(R.id.tvMicrosoftLink);
        tv3.setText( Html.fromHtml("<a href=\"http://blogs.msdn.com/b/translation/p/gettingstarted1.aspx\">MSDN.com</a>"));
        tv3.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)  {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_settings:

                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);

                startActivity(i);

                return true;


            case R.id.action_about:
                Intent i2 = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i2);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }


}