package com.example.opendaytranslator;

import com.memetix.mst.detect.Detect;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

public class BingAPI extends TranslatorAPI {
	private Language[] langArray;
	
	public BingAPI(){
		langArray = Language.values();
       
	}
	
	@Override
	public String[] getLanguages() {
		String language[] = new String[langArray.length];
        for(int i = 0; i < langArray.length; i++){
            language[i] = langArray[i].name();
        }
        return language;
	}

	@Override
	public String [] translateText(String inputText, Language from, Language to) {
		String [] translatedText = new String[2]; 
		
		try {
			
			translatedText[0] = Translate.execute(inputText, from, to);
			
			Language detectedLanguage = Detect.execute(inputText);
		    translatedText[1] = detectedLanguage.getName(Language.ENGLISH);
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	    return translatedText;
	}

}
