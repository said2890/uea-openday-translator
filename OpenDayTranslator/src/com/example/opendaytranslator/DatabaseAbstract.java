package com.example.opendaytranslator;

import java.util.Collection;

public interface DatabaseAbstract {
	public void addRow(Words row);
	public Collection<?> getAllRows();
//	public void removeRow(Words row);
	public void deleteRows();
}
