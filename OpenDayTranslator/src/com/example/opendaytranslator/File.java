package com.example.opendaytranslator;

import java.util.ArrayList;
import java.util.Collection;

import android.content.Context;

import com.kilobolt.framework.FileImplementation;


public class File implements DatabaseAbstract  {

	private FileImplementation FileImplementation;
	private String Filename;
	private Context Context;
	
	public File(Context context, String filename, FileImplementation fileImplementation) {
		Filename = filename;
		Context = context;
		FileImplementation = fileImplementation;
	}
	
	@Override
	public void addRow(Words row) {
		FileImplementation.write(Filename, row.getWord() + ", " + row.getTranslation());
	}

	@Override
	public Collection<?> getAllRows() {
		String [] lines = FileImplementation.read(Filename).split(System.getProperty("line.separator"));
		
		ArrayList<Words> array = new ArrayList<Words>();
		
		for(int i=0; i<lines.length; i++){
			String [] word_translation = lines[i].split(",");
			array.add(new Words(word_translation[0].trim(), word_translation[1].trim()));
		}
		
		return array;
	}

	@Override
	public void deleteRows() {
		Context.deleteFile(Filename);
	}


}
