package com.example.opendaytranslator;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.*;
import android.widget.*;
import com.kilobolt.framework.implementation.Copyright;
import com.kilobolt.framework.implementation.InternalFileImp;
import com.memetix.mst.language.Language;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View.OnClickListener;
import com.memetix.mst.translate.Translate;


public class MainActivity extends Activity implements OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private VoiceProcessor vp = VoiceProcessor.getInstance();
	private RecentWords rw = new RecentWords(new SQLite(this), this);
	//private RecentWords rw = new RecentWords(new File(this, "myFile", new InternalFileImp(this)), this);
	
	public Spinner enteredLanguage, translateLanguage;
	public Button send;
	public TextView languageEntered, textEntered, languageTranslated, textTranslated;
	public EditText userText;
	public ProgressBar translating;
	public TranslatorAPI translatorAPI;
	public Language[] langArray = Language.values();
	public int inputLangID;
	public int outputLangID;
    public String appID;
    public String appKey;
    public Boolean customAPIValuesEnabled;
	public String detectedLanguage = "";
    public boolean InternetIsActive = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main1);

        // AbstractFactory, choose between BingAPI or GoogleAPI
        this.translatorAPI = new BingAPI();
        // VoiceProcessor initialization
        this.vp.initializeProcessor(getApplicationContext());
        // List of translated words
        this.rw.ShowRecentWords();
        // Copyright info positioned at the bottom
        this.ShowCopyright();

		initSettings();
        initViews();
	}



    public void initSettings() {

        //shared preferences for SettingsActivity
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);


        String inputLang;
        String outputLang;

        inputLang = prefs.getString("default_input_lang", "");
        outputLang = prefs.getString("default_output_lang", "");

        if (inputLang.equals(""))  {
            inputLangID = 0; //if settings are empty, auto-detection is the default option for input
        }

        else {
            inputLangID  = Integer.parseInt(inputLang);

        }

        if (outputLang.equals(""))  {
            outputLangID = 12; //if settings are empty, French is the default option for output
        }

        else {
            outputLangID = Integer.parseInt(outputLang);
        }
        //changed

        customAPIValuesEnabled = prefs.getBoolean("key_custom_api", false);
        String customAppID;
        String customAppKey;

        if (!customAPIValuesEnabled)  {
            customAppID = prefs.getString("app_id", "");
            customAppKey = prefs.getString("app_key", "");
            if (customAppID.equals(""))  {
                appID = "ueatranslate2014";
                Translate.setClientId(appID);
            }
            else {
                appID  = customAppID;
                Translate.setClientId(appID);
                // appKey = prefs.getString("app_key", "");
            }

            if (customAppKey.equals(""))  {
                appKey = "GxWb8xjY3ujvD8ggSStbsdXw0yXWROnnkWlCE1LnSAk=";
                Translate.setClientSecret(appKey);
            }
            else {
                // appID  = prefs.getString("app_id", "");
                appKey = customAppKey;
                Translate.setClientSecret(appKey);
            }

        }

        else {
            appID = "ueatranslate2014";
            Translate.setClientId(appID);
            appKey = "GxWb8xjY3ujvD8ggSStbsdXw0yXWROnnkWlCE1LnSAk=";
            Translate.setClientSecret(appKey);
        }

    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
	}
	
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item)  {

             switch (item.getItemId()) {   //starts settings activity
                 case R.id.action_settings:
                     Intent settings = new Intent(getApplicationContext(), SettingsActivity.class);
                     startActivity(settings);
                     return true;
                 case R.id.action_about: //is supposed to start "about" activity.
                     Intent about = new Intent(getApplicationContext(), AboutActivity.class); //change this to call "About" activity
                     startActivity(about);
                     return true;
                 case R.id.clear_history:
                     this.rw.ClearList();
                 default:
                     return super.onOptionsItemSelected(item);
             }
	}


    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals("default_output_lang")) {

            outputLangID = Integer.parseInt((prefs.getString("default_output_lang", "")));
        }

        if (key.equals("default_input_lang")) {
            inputLangID =  Integer.parseInt((prefs.getString("default_input_lang", "")));

        }

        if (key.equals("key_custom_api")) {
            customAPIValuesEnabled = prefs.getBoolean("key_custom_api", false);
        }

        if (key.equals("app_id")) {
            appID = prefs.getString("app_id", "");
        }

        if (key.equals("app_key")) {
            appKey = prefs.getString("app_key", "");
        }



    }


    public void initViews(){
        // Speaker Buttons
        Button speakerButton = ((Button) findViewById(R.id.speak_button));
        Button speakerButton1 = ((Button) findViewById(R.id.speak_button1));
        speakerButton.setOnClickListener(this);
        speakerButton1.setOnClickListener(this);

        enteredLanguage = (Spinner) findViewById(R.id.spinnerTrFrom);
        translateLanguage = (Spinner) findViewById(R.id.spinnerTrTo);

        enteredLanguage.setAdapter(new ArrayAdapter<String>(this,
         		android.R.layout.simple_spinner_dropdown_item, translatorAPI.getLanguages()));
        enteredLanguage.setSelection(inputLangID); //ch


        translateLanguage.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, translatorAPI.getLanguages()));

        translateLanguage.setSelection(outputLangID); //ch

        send = (Button) findViewById(R.id.imgButtonTranslate);
        languageEntered = (TextView) findViewById(R.id.textInputLanguageName);
        textEntered = (TextView) findViewById(R.id.inputText);
        languageTranslated = (TextView) findViewById(R.id.textTranslatedLanguageName);
        textTranslated = (TextView) findViewById(R.id.translatedText);
        userText = (EditText) findViewById(R.id.editEnteredText);

        //Visibility options..
        languageEntered.setVisibility(TextView.INVISIBLE);
        languageTranslated.setVisibility(TextView.INVISIBLE);
        textEntered.setVisibility(TextView.INVISIBLE);
        textTranslated.setVisibility(TextView.INVISIBLE);

        translating = (ProgressBar) findViewById(R.id.progressBarTranslating);
        translating.setVisibility(ProgressBar.INVISIBLE);
        ((View) findViewById(R.id.view1)).setVisibility(View.INVISIBLE);
        ((View) findViewById(R.id.view2)).setVisibility(View.INVISIBLE);

        send.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

         /* Creating Async Task - It is used to make translation process independent from the user interface,
         in case if translation takes too long (poor connection, many words in input, etc..)
          */
                class asyncTranslate extends AsyncTask<Void, Void, Void>{

                    String [] translatedText = new String[2];

                    @Override
                    protected void onPreExecute() {
                        /* Checking Internet connection */
                        ConnectivityManager connectivityManager
                                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

                        if(activeNetworkInfo == null) {
                            languageEntered.setVisibility(TextView.VISIBLE);
                            languageTranslated.setVisibility(TextView.INVISIBLE);
                            textEntered.setVisibility(TextView.INVISIBLE);
                            textTranslated.setVisibility(TextView.INVISIBLE);
                            ((View) findViewById(R.id.speak_button)).setVisibility(View.INVISIBLE);
                            ((View) findViewById(R.id.speak_button1)).setVisibility(View.INVISIBLE);

                            languageEntered.setText(getString(R.string.internet_required));
                            InternetIsActive = false;
                        }

                        else {
                            InternetIsActive = true;
                            translating.setVisibility(ProgressBar.VISIBLE);
                            super.onPreExecute();
                        }
                    }

                    /* doInBackground - It will do the translation using an independent thread */

                    @Override
                    protected Void doInBackground(Void... params) {
                            try {
                                translatedText = translatorAPI.translateText(userText.getText().toString(), langArray[enteredLanguage.getSelectedItemPosition()], langArray[translateLanguage.getSelectedItemPosition()]);
                                detectedLanguage = translatedText[1];
                            } catch (Exception e) {
                                translatedText[0] = "Translation Failed!";
                            }
                            return null;
                    }

                       /* onPostExecute - Runs on the UI thread after translation is completed */

                    @Override
                    protected void onPostExecute(Void result) {
                        if(InternetIsActive) {
                            try {
                            languageEntered.setVisibility(TextView.VISIBLE);
                            languageTranslated.setVisibility(TextView.VISIBLE);
                            textEntered.setVisibility(TextView.VISIBLE);
                            textTranslated.setVisibility(TextView.VISIBLE);

                            ((View) findViewById(R.id.view1)).setVisibility(View.VISIBLE);
                            ((View) findViewById(R.id.view2)).setVisibility(View.VISIBLE);

                            textEntered.setText(userText.getText().toString());
                            textTranslated.setText(translatedText[0]);

                            languageEntered.setText(detectedLanguage);
                            languageTranslated.setText(langArray[translateLanguage.getSelectedItemPosition()].name());

                            /****************Add button if language is English************************/
                            if(languageEntered.getText().toString().toLowerCase().equals(getString(R.string.english).toLowerCase()))
                                ((View) findViewById(R.id.speak_button)).setVisibility(View.VISIBLE);
                            else ((View) findViewById(R.id.speak_button)).setVisibility(View.INVISIBLE);

                            if(languageTranslated.getText().toString().toLowerCase().equals(getString(R.string.english).toLowerCase()))
                                ((View) findViewById(R.id.speak_button1)).setVisibility(View.VISIBLE);
                            else ((View) findViewById(R.id.speak_button1)).setVisibility(View.INVISIBLE);
                            /*************************************************************************/

                            // add words to the list of recent words
                            String langTrans = " (" + languageTranslated.getText().toString().substring(0, 2).toLowerCase() + ")";
                            String langEnt = " (" + languageEntered.getText().toString().substring(0, 2).toLowerCase() + ")";

                            rw.addWord(new Words(textEntered.getText().toString() + langEnt,
                                    textTranslated.getText().toString() + langTrans));

                            translating.setVisibility(ProgressBar.INVISIBLE);
                            super.onPostExecute(result);
                            }
                            catch (Exception e) {
                                translatedText[0] = "Translation Failed Because of API Issues!";
                            }
                        }
                    }
                }
                new asyncTranslate().execute();
            }
        });
    }
	    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
        switch(v.getId()){
            case R.id.speak_button:
                vp.speak(textEntered.getText().toString());
                break;
            case R.id.speak_button1:
                vp.speak(textTranslated.getText().toString());
                break;
        }


		//String word = rw.getAllWords().get(v.getId()).getWord();
		//String translation = rw.getAllWords().get(v.getId()).getTranslation();
		
		//Log.d("Recent Words: Test Click", word + translation);
	}


    public void ShowCopyright(){
        // Copyright, e.g. "Author1, Author2 and Author 3"
        String authors = getString(R.string.timur) + " " + getString(R.string.and) + " " + getString(R.string.said);
        Copyright copyright = new Copyright(this, authors);
        TextView tv = (TextView) findViewById(R.id.tvCopyright);
        tv.setText(copyright.getCopyright(this));
    }

}
