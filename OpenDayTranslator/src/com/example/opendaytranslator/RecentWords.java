package com.example.opendaytranslator;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RecentWords {
	
	private DatabaseAbstract Database;
	private ArrayList<Words> recentWords;
	private Activity Activity;
    private final int TABLE_LAYOUT_NAME = R.id.recentwords;

	public RecentWords(DatabaseAbstract database, Activity activity) {
		Database = database;
        Activity = activity;
		recentWords = new ArrayList<Words>();
	}
	
	public void addWord(Words words){
		Database.addRow(words);
		recentWords.add(words);
        this.RemoveRecentWords();
        this.ShowRecentWords();
	}
	
	public ArrayList<Words> getAllWords(){
		return recentWords;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Words> getAllWordsFromDatabase(){
		recentWords = (ArrayList<Words>) Database.getAllRows();
		Log.d("RecentWords", "successful");
		return recentWords;
	}

	public void ClearList() {
		Database.deleteRows();
		recentWords.clear();
        this.RemoveRecentWords();
        this.ShowRecentWords();
	}

    public void RemoveRecentWords() {
        TableLayout tl = (TableLayout) Activity.findViewById(TABLE_LAYOUT_NAME);
        tl.removeAllViews();
    }

    public void ShowRecentWords() {
        // displaying info from the sqlite database
        // get the TableLayout
        TableLayout tl = (TableLayout) Activity.findViewById(TABLE_LAYOUT_NAME);
        // create RecentWords object

        //rw.addWord(new Words("hi", "bonjour"));

        //rw.ClearList();
        ArrayList<Words> array = this.getAllWordsFromDatabase();

        // go through each item in the arrayjh
        for (int i = array.size()-1; i >= 0; i--) {

            // create a TableRow and give it an ID
            TableRow tr = new TableRow(Activity.getApplicationContext());
            tr.setId(i);
            tr.setPadding(20, 20, 0, 20);
            //tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

            // create a TextView to house the word
            TextView tc1 = new TextView(Activity.getApplicationContext());
            tc1.setId(200 + i);
            tc1.setTextSize(20);
            tc1.setText(array.get(i).getWord());
            //tc1.setPadding(20, 0, 0, 0);
            tc1.setTextColor(Color.BLACK);
            //tc1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            tr.addView(tc1);

            // create a TextView to house the word
            TextView tc2 = new TextView(Activity.getApplicationContext());
            tc2.setId(100 + i);
            tc2.setTextSize(20);
            tc2.setText(array.get(i).getTranslation());
            tc2.setTextColor(Color.BLACK);
            //tc2.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            tr.addView(tc2);

            Log.d("RecentWords", "I am here");
            // add the TableRow to the TableLayout
            tl.addView(tr, new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            Log.d("RecentWords", "Over here");
        }
    }
}
