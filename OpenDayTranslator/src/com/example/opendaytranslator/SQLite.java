package com.example.opendaytranslator;

import java.util.ArrayList;
import java.util.Collection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



public class SQLite extends SQLiteOpenHelper implements DatabaseAbstract {

	// Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "RecentWordsDB";
    // Table name
    private static final String TABLE_NAME = "recent_words";
    // Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_FROM_LANG = "from_language";
    private static final String KEY_TO_LANG = "to_language";

    //private static final String[] COLUMNS = {KEY_ID, KEY_FROM_LANG, KEY_TO_LANG};
    
	
	public SQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);  
    }
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		// SQL statements to create the table
		String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +" ( " +
							KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
							KEY_FROM_LANG + " TEXT, " +
							KEY_TO_LANG + " TEXT )";
		// create table
		db.execSQL(CREATE_TABLE);
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		
		// create fresh table
		this.onCreate(db);
		
	}
	

	@Override
	public void addRow(Words row) {
		// logging
		Log.d("Recent words: add ", row.toString());
		
		// get reference to writable database
		SQLiteDatabase db = this.getWritableDatabase();
		
		// create ContentValues to add key "column"/value
		ContentValues values = new ContentValues();
		values.put(KEY_FROM_LANG, row.getWord());
		values.put(KEY_TO_LANG, row.getTranslation());
		
		// insert 
		db.insert(TABLE_NAME, null, values);
	
		// close
		db.close();
	}
	

	@Override
	public Collection<?> getAllRows() {
		ArrayList<Words> words = new ArrayList<Words>();
		
		// the query
		String query = "SELECT * FROM " + TABLE_NAME;
		
		// get reference to writable database
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		
		// go over each row, build Words and add to the list
		if (cursor.moveToFirst()){
			do {
				words.add(new Words(cursor.getString(1), cursor.getString(2)));
			} while (cursor.moveToNext());
		}
		
		// logging
		Log.d("Recent words: get all", words.toString());
		
		return words;
	}


	@Override
	public void deleteRows() {
		// get reference to writable database
		SQLiteDatabase db = this.getWritableDatabase();
		
		// delete all rows 
		db.delete(TABLE_NAME, null, null);
		
		// logging
		Log.d("Recent Words", "recent words are cleared");
		
	}


}
