package com.example.opendaytranslator;

/**
 * Created by timur on 07/04/2014.
 */


import android.content.Intent;
import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import java.util.List;

public class SettingsActivity extends PreferenceActivity {


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

}

    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preferences_headers, target);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)  {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_settings:

                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);

                startActivity(i);

                return true;


            case R.id.action_about:
                Intent i2 = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(i2);
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }


}