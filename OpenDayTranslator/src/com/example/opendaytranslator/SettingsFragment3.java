package com.example.opendaytranslator;

/**
 * Created by timur on 07/04/2014.
 */


import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsFragment3 extends PreferenceFragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences3);
    }

}
