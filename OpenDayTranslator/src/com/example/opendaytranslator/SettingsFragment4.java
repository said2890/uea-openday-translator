package com.example.opendaytranslator;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by Samada San on 24/04/14.
 */
public class SettingsFragment4 extends PreferenceFragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.translation_api);
    }

}