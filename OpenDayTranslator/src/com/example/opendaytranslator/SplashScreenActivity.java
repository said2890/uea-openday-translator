package com.example.opendaytranslator;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.kilobolt.framework.implementation.SplashScreen;

import android.os.Bundle;
import android.app.Activity;


public class SplashScreenActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public Boolean vibrationIsOff;
    public Boolean soundIsOff;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //shared preferences for SettingsActivity
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

		// a class to play a sound and make the activity sleep for 3500 ms
		SplashScreen introScreen = new SplashScreen(SplashScreenActivity.this, this, MainActivity.class, R.layout.activity_splash_screen, 3500);

        vibrationIsOff = prefs.getBoolean("key_vibration", false);
        soundIsOff = prefs.getBoolean("key_sound", false);

        if (!vibrationIsOff) {
            introScreen.vibrate(700, 2500);
        }

        if (!soundIsOff) {
            introScreen.play("intro.mp3");
        }

        introScreen.show();

	}

    @Override
    protected void onPause(){
        finish();
        super.onPause();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

        if (key.equals("key_vibration")) {
         vibrationIsOff = prefs.getBoolean("key_vibration", false);
        }
         if (key.equals("key_sound")) {
         soundIsOff = prefs.getBoolean("key_sound", false);
        }
    }

 }
