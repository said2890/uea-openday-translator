package com.example.opendaytranslator;

import com.memetix.mst.language.Language;

public abstract class TranslatorAPI {
	public abstract String[] getLanguages();
	public abstract String[] translateText(String inputText, Language from, Language to);
}
