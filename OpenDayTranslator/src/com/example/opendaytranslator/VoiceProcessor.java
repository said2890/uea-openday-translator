package com.example.opendaytranslator;

import android.content.Context;
import android.view.View;
import com.external.opendaytranslator.TextToSpeechModel;

public class VoiceProcessor {
    private static VoiceProcessor instance = null;
    //private Context Context;
    private TextToSpeechModel ttsm;

    private VoiceProcessor() {
        // Exists only to defeat instantiation
    }

    public static VoiceProcessor getInstance() {
        if(instance==null) {
            instance = new VoiceProcessor();
        }
        return instance;
    }

    public void initializeProcessor(Context context) {
            ttsm = new TextToSpeechModel(context);
    }

    public void speak(String text){
         ttsm.speak(text);
    }

    public TextToSpeechModel setSettings(){
        return ttsm;
    }
}
