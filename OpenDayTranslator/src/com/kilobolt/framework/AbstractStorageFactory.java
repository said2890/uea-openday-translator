package com.kilobolt.framework;

import android.app.Activity;

public abstract class AbstractStorageFactory {
	protected Activity Act;
	
	public AbstractStorageFactory(Activity TheAct) {
		Act = TheAct;
	}
	
	public abstract FormatStrategy getStrategy();
	public abstract FileImplementation getFileImp();
}
