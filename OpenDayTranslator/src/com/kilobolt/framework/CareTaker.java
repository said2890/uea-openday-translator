package com.kilobolt.framework;

import java.util.ArrayList;

/**
 * This is a container class for Memento class instances.
 * 
 * @author Said-Magomed Sadulaev
 *
 */

public class CareTaker {
	private ArrayList <Memento> savedStates = new ArrayList <Memento>();
	
	public void addMemento(Memento m){
		savedStates.add(m);
	}
	
	public Memento getMemento(int index){
		return savedStates.get(index);
	}
}
