package com.kilobolt.framework;

import android.app.Activity;

/**
 * <p>Abstract class, which provides methods to write and read text from a file.</p>
 *
 * <p>This class provides the abstraction for storing files on either internal or external storage. </p>
 *
 * @author Said-Magomed Sadulaev
 *
 */
public abstract class FileImplementation {
	protected Activity Act;

    /**
     * Constructor
     *
     * @param TheAct    the activity, which requires storing and reading from a file
     *                  e.g. new FileImplementation(this)
     */
	protected FileImplementation(Activity TheAct){
		Act = TheAct;
	}

    /**
     * Reads and returns text from a specified file @param FileName in a String
     *
     * @param FileName  a path to the file to read from
     * @return text from the file
     */
	public abstract String read(String FileName);

    /**
     * Writes text @param Contents into the specified file
     *
     * @param FileName a path to the file to write to
     * @param Contents the text to be written/added/substituted into the file
     *
     * @return true if the text has been successfully written to the specified file
     *         false otherwise
     */
	public abstract boolean write(String FileName, String Contents);
}
