package com.kilobolt.framework;

public abstract class FormatStrategy {
	public abstract String Format(String Heading, String Body);
}
