package com.kilobolt.framework;

import android.content.Context;

import com.example.opendaytranslator.R;

/**
 * This is a reusable abstract class to display copyright information
 * 
 * @author Said-Magomed Sadulaev
 *
 *
 */
public abstract class FrameworkCopyright {

    /**
     * <p>Returns copyright information of the framework and the application. The application copyright information is
     * returned by abstract getAppCopyright() and is set in a concrete child class "Copyright". The parameter should be
     * passed from an activity and is the context of current state of the application. </p>
     *
     * @param context e.g.  FrameworkCopyright copyright = new FrameworkCopyright(this);
     * @return the framework copyright information concatenated with the copyright information of the application.
     * The latter is set in a concrete child class "Copyright".
     *
     */

	public final String getCopyright(Context context) {
		return context.getString(R.string.copyright_saaf) + "\n\n" + getAppCopyright();
	}

    /**
     *<p>Is an abstract class, which returns the application copyright information and is set in a concrete
     * child class</p>
     *
     * @return application copyright information
     */
	protected abstract String getAppCopyright();
	
}
