package com.kilobolt.framework;

/**
 *
 * This abstract class contains the actual state information
 * 
 * @author Said-Magomed Sadulaev
 *
 *
 */
public class Memento {
	private MementoState state;
	
	public Memento(MementoState newState){
		state = newState;
	}
	
	public MementoState getState(){
		return state;
	}
}
