package com.kilobolt.framework;

/**
 * 
 * This class contains the actual state of an object.
 * It is abstract as for each specific application a 
 * different type of state information can be used 
 * by inheriting from this class.
 * 
 * @author Said-Magomed Sadulaev
 *
 */

public abstract class MementoState {
	
}
