package com.kilobolt.framework;

/**
 * 
 * @author Said-Magomed Sadulaev
 *
 */
public abstract class Observer {
	public abstract void Update();
	
}
