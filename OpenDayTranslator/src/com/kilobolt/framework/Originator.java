package com.kilobolt.framework;

/**
 * This is the class that has a state worth saving using the Memento pattern. 
 * This class is abstract as applications will need to refine it to 
 * determine exactly what kind of state they would like to save.
 * 
 * @author Said-Magomed Sadulaev
 *
 */

public abstract class Originator {
	protected MementoState state;
	
	public final void setState(MementoState state){
		this.state = state;
	}
	
	public final Memento saveToMemento() {
		return new Memento(state);
	}
	
	public final void restoreFromMemento(Memento m){
		state = m.getState();
	}
	
}
