package com.kilobolt.framework;

import com.kilobolt.framework.implementation.AndroidAudio;

import android.app.Activity;

/***
 * SoundResource can be used to play any sound effect
 * <p>
 * You first need to load a resource to play
 * and then call the play method.
 * <p>
 * Instantiate the class, call {@link #load(String)} and {@link #play()} where required
 * 
 * @author Said-Magomed Sadulaev
 * @see	#load(String)
 * @see #play()
 */
public class SoundResource {
	private Audio MyAudio;
	private Sound MySound;
	
	/***
	 * 
	 * @param Act		which is an activity a sound should be played in (calling activity)
	 *
	 * Example: 	SoundResource sr = new SoundResource(this);
	 */
	public SoundResource(Activity Act) {
		MyAudio = new AndroidAudio(Act);
	}
	
	/**
	 * 
	 * @param resourcePath		the name of a sound file, which is stored in "assets" folder
	 * 
	 * Example:		soundResource.load("click.mp3"); 	
	 *
	 * 
	 */
	public void load(String resourcePath){
		MySound = MyAudio.createSound(resourcePath);
	}
	

	public void play(){
		MySound.play((float)0.9);
	}
	
	public void stop(){
		MySound.stop();
	}
}
