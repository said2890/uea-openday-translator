package com.kilobolt.framework;

import java.util.HashSet;
import java.util.Iterator;


/**
 * 
 * @author Said-Magomed Sadulaev
 *
 */
public abstract class Subject {
	private HashSet<Observer> observers = new HashSet<Observer>();
	
	public final void Attach(Observer obs){
		if(!observers.contains(obs)){
			observers.add(obs);
		}
	}
	
	public final void Detach(Observer obs){
		if(observers.contains(obs)) {
			observers.remove(obs);
		}
	}
	
	protected final void Notify(){
		Iterator<Observer> ObIt = observers.iterator();
		
		while(ObIt.hasNext()){
			ObIt.next().Update();
		}
	}
}
