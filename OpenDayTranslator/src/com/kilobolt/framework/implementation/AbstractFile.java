package com.kilobolt.framework.implementation;

import com.kilobolt.framework.FileImplementation;

public class AbstractFile {
	private FileImplementation FileImp;
	
	protected AbstractFile(FileImplementation Imp){
		FileImp = Imp;
	}
	
	protected final String read(String FileName){
		return FileImp.read(FileName);
	}
	
	protected final boolean write(String FileName, String Contents){
		return FileImp.write(FileName, Contents);
	}
	
	public void setImplementation(FileImplementation NewImp){
		FileImp = NewImp;
	}
}
