package com.kilobolt.framework.implementation;

import com.kilobolt.framework.Observer;

/**
 * This is a generic ConcreteObserver class.
 * 
 * It keeps a reference to the Subject it 
 * is connected to (MySubject).
 * 
 * Also it has a member variable to which it 
 * forwards the updated subject state (MyClient). 
 * The reason for this is to separate the handling 
 * of the state (e.g. displaying it on a widget) 
 * so that it remains generic.
 * 
 * @author Said-Magomed Sadulaev
 *
 * @param <T> A generic state of a subject (String, integer, class etc.)
 */

public final class ConcreteObserver<T> extends Observer {
	private ConcreteSubject<T> MySubject;
	private ObserverClient<T> MyClient;
	
	/**
	 * 
	 * @param Subject		a subject this observer is connected to
	 * @param Client		a client class that implements the ObserverClient interface
	 */
	public ConcreteObserver(ConcreteSubject<T> Subject, ObserverClient<T> Client) {
		MySubject = Subject;
		MySubject.Attach(this);
		MyClient = Client;
	}
	
	public void Detach() {
		MySubject.Detach(this);
	}
	
	/**
	 * forwarding the updated subject state to
	 * the observer client
	 */
	@Override
	public void Update() {
		MyClient.Update(MySubject.GetState());
	}
}
