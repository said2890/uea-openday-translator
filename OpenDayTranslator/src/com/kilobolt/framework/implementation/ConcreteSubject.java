package com.kilobolt.framework.implementation;
import com.kilobolt.framework.Subject;

/**
 * The class is generic because the actual state
 * of a subject can take many different forms, 
 * i.e. it can be integer, string or self-defined
 * classes.
 * 
 * @author Said-Magomed Sadulaev
 *
 * @param <T>	A generic state of a subject (String, integer, class etc.)
 */
public final class ConcreteSubject<T> extends Subject {
	private T State;
	
	public T GetState(){
		return State;
	}
	
	/**
	 * Sets the new state and notifies the attached 
	 * observers that its state has been changed
	 * 
	 * @param newState	
	 */
	public void SetState(T newState){
		State = newState;
		this.Notify();
	}
}
