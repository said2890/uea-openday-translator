package com.kilobolt.framework.implementation;


import android.content.Context;

import com.example.opendaytranslator.R;
import com.kilobolt.framework.FrameworkCopyright;

/**
 *
 * @author Said-Magomed Sadulaev
 */
public class Copyright extends FrameworkCopyright {
	private String Authors;
	private Context Context;

    /**
     * Constructor
     *
     * @param context current state of the application, e.g. Copyright copyright = new Copyright(this, authors);
     * @param authors  a group of people who has created the application
     */
	public Copyright(Context context, String authors) {
		Context = context;
		Authors = authors;
	}

    /**
     *<p>This method adds authors of the application to the copyright information</p>
     *
     * @return copyright information of the application in String
     *
     */
	@Override
	protected String getAppCopyright() {
			return Context.getString(R.string.created_by) + " " + Authors + ", (c) 2014";
	}

	
}
