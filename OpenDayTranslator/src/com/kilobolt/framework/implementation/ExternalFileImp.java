package com.kilobolt.framework.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.kilobolt.framework.FileImplementation;

import android.app.Activity;
import android.os.Environment;

/**
 * This is a realisation of an abstract FileImplementation class and stores files on external storage.
 *
 * @author Said-Magomed Sadulaev
 */
public class ExternalFileImp extends FileImplementation {
	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param TheAct
     */
	public ExternalFileImp(Activity TheAct) {
		super(TheAct);
	}

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param FileName  a path to the file to read from
     * @exception java.io.FileNotFoundException throws when a file is not found in the specified path @param FileName
     * @exception java.io.IOException throws when there is an error whilst reading the file
     *
     * @return  contents of the file in a String
     */
	@Override
	public String read(String FileName) {
		UpdateExternalStorageState();
	
		if (mExternalStorageAvailable) {
			File file = new File(Act.getExternalFilesDir(null), FileName);
			//getExternalFilesDir(null) returns the root of the external storage
			FileInputStream fis;
			
			try {
				fis = new FileInputStream(file);
			} catch (IOException e) {
				e.printStackTrace();
				return "ERROR_FILE_DOES_NOT_EXIST_ON_EXT";
			}
			
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader bufferedReader = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String line;
			
			try {
				while ((line = bufferedReader.readLine()) != null) {
					sb.append(line);
				}
				return sb.toString();
			} catch (IOException e) {
				e.printStackTrace();
				return "ERROR_WHILE_READING_FILE";
			}
		}
		else {
			return "ERROR_EXTERNAL_STORAGE_NOT_AVAILABLE";
		}
	}

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param FileName a path to the file to write to
     * @param Contents the text to be written/added/substituted into the file
     * @exception java.io.FileNotFoundException throws when a file is not found in the specified path @param FileName
     * @exception java.io.IOException throws when there is an error whilst writing to the file
     *
     * @return true if the file has been successfully updated/rewritten
     */
	@Override
	public boolean write(String FileName, String Contents) {
		UpdateExternalStorageState();
		if (mExternalStorageWriteable) {
			File file = new File(Act.getExternalFilesDir(null), FileName);
			//getExternalFilesDir(null) returns the root of the external storage
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(file);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
			try {
				fos.write(Contents.getBytes());
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		else {
			return false;
		}
		return true;
	}
	
	
	private void UpdateExternalStorageState() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something is wrong, we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
	}

}
