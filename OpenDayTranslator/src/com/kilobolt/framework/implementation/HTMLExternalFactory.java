package com.kilobolt.framework.implementation;

import android.app.Activity;

import com.kilobolt.framework.AbstractStorageFactory;
import com.kilobolt.framework.FileImplementation;
import com.kilobolt.framework.FormatStrategy;

public class HTMLExternalFactory extends AbstractStorageFactory {

	public HTMLExternalFactory(Activity TheAct) {
		super(TheAct);
	}
	
	@Override
	public FormatStrategy getStrategy() {
		return new HTMLStrategy();
	}
	
	@Override
	public FileImplementation getFileImp() {
		return new ExternalFileImp(Act);
	}
	
}
