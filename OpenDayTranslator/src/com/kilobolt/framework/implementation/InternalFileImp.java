package com.kilobolt.framework.implementation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import com.kilobolt.framework.FileImplementation;

import android.app.Activity;
import android.content.Context;

/**
 * This is a realisation of an abstract FileImplementation class and stores files on internal storage.
 *
 * @author Said-Magomed Sadulaev
 */

public class InternalFileImp extends FileImplementation {

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param TheAct
     */
	public InternalFileImp(Activity TheAct){
		super(TheAct);
	}

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param FileName  a path to the file to read from
     * @exception java.io.FileNotFoundException throws when a file is not found in the specified path @param FileName
     * @exception java.io.IOException throws when there is an error whilst reading the file
     *
     * @return  contents of the file in a String
     */
	@Override
	public String read(String FileName){
		FileInputStream fis;
		try{
				fis = Act.openFileInput(FileName);
		}catch(FileNotFoundException e) {
				e.printStackTrace();
				return "ERROR_FILE_NOT_FOUND";
		}
		
		InputStreamReader isr = new InputStreamReader(fis);
		BufferedReader bufferedReader = new BufferedReader(isr);
		StringBuilder sb = new StringBuilder();
		String line;
		try{
				while((line = bufferedReader.readLine())!=null){
					sb.append(line);
				}
				return sb.toString();
		}catch(IOException e){
				e.printStackTrace();
				return "ERROR_WHILE_READING_FILE";
		}
	}

    /**
     * @see com.kilobolt.framework.FileImplementation
     *
     * @param FileName a path to the file to write to
     * @param Contents the text to be written/added/substituted into the file
     * @exception java.io.FileNotFoundException throws when a file is not found in the specified path @param FileName
     * @exception java.io.IOException throws when there is an error whilst writing to the file
     *
     * @return true if the file has been successfully updated/rewritten
     */
	@Override
	public boolean write(String FileName, String Contents) {
		FileOutputStream fos;
		String existingContents;
		
		try {
			// CHANGE
				existingContents = this.read(FileName);
				fos = Act.openFileOutput(FileName, Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
			}
		try {
			// CHANGE
				if(!existingContents.contentEquals("")|| !existingContents.contentEquals("ERROR_FILE_NOT_FOUND")) Contents = existingContents + "\n" + Contents;
				fos.write(Contents.getBytes());
				fos.close();
				return true;
		} catch (IOException e) {
				e.printStackTrace();
				return false;
		}
	}
}
