package com.kilobolt.framework.implementation;

public interface ObserverClient<T> {
	public void Update(T StateUpdate);
}
