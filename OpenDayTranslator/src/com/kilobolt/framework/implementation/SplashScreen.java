package com.kilobolt.framework.implementation;

import com.kilobolt.framework.SoundResource;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Vibrator;


public class SplashScreen {

    /**
     * This class includes SplashScreen constructor and methods that are used for displaying the splash screen activity (which runs when the application starts).
     * <p>Is used for showing the splash screen logo (UEA logo), for playing sound effects and for the vibration effect.</p>
     *
     */

    private Thread UEAIntroThread;
    private SoundResource introSound;
    private Activity activity;
    private Context context;

    /**
     * This constructor provides threads for displaying splash screen logo and for playing the intro sound.
     * It uses Android Context features for passing the context of this constructor to other classes.
     * Splash screen logo is shown for a certain time (countdown algorithm for this feature is provided below).
     * The default value for countdown is 3500 ms, and set in SplashScreenActivity.
     * This structure is used to make application more extensible, keeping generic parts of the code in SAAF (Framework).
     *
     * @param activity The activity where the splash screen is supposed to be used
     * @param context  Context of the application (i.e. "this")
     * @param activity_class
     * @param layout The layout where related items are displayed
     * @param time Time in ms (milliseconds), set in SplashScreenActivity, default value is 3500 ms
     * @see com.example.opendaytranslator.SplashScreenActivity
     */

    public SplashScreen(final Activity activity, final Context context, final Class<?> activity_class, int layout, int time){

        activity.setContentView(layout);
        this.activity = activity;
        this.context = context;

	        /*
            Setting time (How long the intro screen should be displayed) */

        final int introScreenDisplayTime = time;

        // Loading Sound and playing it
        introSound = new SoundResource(activity);


	        /* Starting a new thread - it is active until time we set runs out */


        UEAIntroThread = new Thread() {

            int timer = 0;

            @Override
            public void run() {
                try {
                    super.run();

                    while (timer < introScreenDisplayTime) {
                        sleep(100);
                        timer += 100;
                    }
                } catch (Exception e) {
                    System.out.println("Intro Screen Activity exception caught:" + e);
                } finally {
                    activity.startActivity(new Intent(context, activity_class));
                    activity.finish();
                }
            }
        };


    }


    public void show() {
        UEAIntroThread.start();
    }

    /**
     *
     * @param time
     * @param delay
     */

    public void vibrate(final long time, final long delay) {
        final Vibrator myVib = (Vibrator) activity.getSystemService(context.VIBRATOR_SERVICE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                myVib.vibrate(time);
            }
        }, delay);
    }

    /**
     *
     * @param sound
     */

    public void play(String sound) {
        // TODO Auto-generated method stub

        introSound.load(sound);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                introSound.play();
            }
        }, 100);
    }

}
