package com.kilobolt.framework.implementation;

import android.app.Activity;

import com.kilobolt.framework.AbstractStorageFactory;
import com.kilobolt.framework.FileImplementation;
import com.kilobolt.framework.FormatStrategy;

public class StringInternalFactory extends AbstractStorageFactory {

	public StringInternalFactory(Activity TheAct) {
		super(TheAct);
	}
	
	@Override
	public FormatStrategy getStrategy() {
		return new StringStrategy();
	}
	
	@Override
	public FileImplementation getFileImp() {
		return new InternalFileImp(Act);
	}
}
